package chapter1;

public class Ex10 {

    public static void main(String[] args) {
        /*
        (Average speed in miles) Assume a runner runs 14 kilometers in 45 minutes and 30
        seconds. Write a program that displays the average speed in miles per hour. (Note
        that 1 mile is 1.6 kilometers.)
         */

        final double MILE_PER_KILOMETER = 0.621371;

        double kilometers = 14;

        double KILOMETERS_IN_MILES = kilometers * MILE_PER_KILOMETER;

        System.out.println(KILOMETERS_IN_MILES);

        double mph = (KILOMETERS_IN_MILES) / 1.4530;

        System.out.println(mph);

    }
}
