package chapter2;

import java.util.Scanner;

public class Ex10 {

    public static void main(String[] args) {

        /*
        (Science: calculating energy) Write a program that calculates the energy needed
        to heat water from an initial temperature to a final temperature. Your program
        should prompt the user to enter the amount of water in kilograms and the initial
        and final temperatures of the water.
         */

        Scanner input = new Scanner(System.in);

        System.out.println("Enter the amount of water in kilograms: ");
        double m = input.nextDouble();
        System.out.println("Enter the initial temperature: ");
        double initialTemperature = input.nextDouble();
        System.out.println("Enter the final temperature: ");
        double finalTemperature = input.nextDouble();

        double q = m * (finalTemperature - initialTemperature) * 4184;

        System.out.println("The energy needed is " + q);
    }
}
