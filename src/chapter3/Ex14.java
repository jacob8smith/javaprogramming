package chapter3;

import java.util.Scanner;

public class Ex14 {

    public static void main(String[] args) {

        /*
        (Game: heads or tails) Write a program that lets the user guess whether the flip of
        a coin results in heads or tails. The program randomly generates an integer 0 or 1,
        which represents head or tail. The program prompts the user to enter a guess and
        reports whether the guess is correct or incorrect.
         */

        Scanner input = new Scanner(System.in);
        int coinToss = (int) (Math.random() * 2);

        System.out.println("Enter your coin flip guess: ");
        int guess = input.nextInt();

        if (coinToss == guess && coinToss == 0) {
            System.out.println("The flip is heads, you are correct");
        }
        else if (coinToss == guess && coinToss == 1) {
            System.out.println("The flip is tails, you are correct");
        }
        else {
            System.out.println("You are incorrect");
        }


    }
}
