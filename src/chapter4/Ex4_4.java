package chapter4;

import java.util.Scanner;

public class Ex4_4 {

    public static void main(String[] args) {

        /*
        (Geometry: area of a hexagon) The area of a hexagon can be computed using the
        following formula (s is the length of a side):
         */

        Scanner input = new Scanner(System.in);

        System.out.println("Enter the side: ");
        double s = input.nextDouble();

        double area = (6 * (s * s)) / (4 * Math.tan(Math.PI / 6));

        System.out.println("The area of a hexagon is " + area);
    }
}
