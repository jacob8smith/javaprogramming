package chapter5;

import java.util.Scanner;

public class Ex1 {

    public static void main(String[] args) {

         /*
         (Count positive and negative numbers and compute the average of numbers) Write
        a program that reads an unspecified number of integers, determines how many
        positive and negative values have been read, and computes the total and average of
        the input values (not counting zeros). Your program ends with the input 0. Display
        the average as a floating-point number.
          */

        Scanner input = new Scanner(System.in);
        System.out.println("Enter an integer, the input ends if it is 0: ");
         int num = input.nextInt();
         int positive = 0;
         int negative = 0;
         int sum = 0;
         int count = 0;

         while (num != 0) {
             num = input.nextInt();
             sum += num;
             if (num < 0) {
                 negative++;
             }
             else if (num > 0) {
                 positive++;
             }
             count++;
         }

         double average = sum / count;

         System.out.println("The number of positives is " + positive);
         System.out.println("The number of negatives is " + negative);
         System.out.println("The total is " + sum);
         System.out.println("The average is " + average);

    }
}
