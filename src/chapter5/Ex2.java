package chapter5;

import java.util.Scanner;

public class Ex2 {

    public static void main(String[] args) {

        /*
        (Repeat additions) Listing 5.4, SubtractionQuizLoop.java, generates five random
        subtraction questions. Revise the program to generate ten random addition questions
        for two integers between 1 and 15. Display the correct count and test time.
         */

        Scanner input = new Scanner(System.in);

        int num1 = (int) Math.random() * 15;
        int num2 = (int) Math.random() * 15;
        int correctCount = 0;
        int incorrectCount = 0;

        for (int i = 1; i <+ 10; i++) {

            System.out.println("What is " + num1 + " + " + num2 + " ?");
            int guess = input.nextInt();

            if (guess == num1 + num2) {
                System.out.println("Your answer is correct");
                correctCount++;
            }
            else {
                System.out.println("Your answer is incorrect");
                incorrectCount++;
            }
        }

        System.out.println("You've got " + correctCount + " answers correct.");
        System.out.println("You've got " + incorrectCount + " answers incorrect.");


    }
}
