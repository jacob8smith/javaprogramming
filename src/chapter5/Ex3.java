package chapter5;

import java.util.Scanner;

public class Ex3 {

    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        System.out.println("Kilograms             Pounds");

        for (int i = 1; i <= 199; i++) {

            System.out.println(i + "                      " + (i * 2.2));

        }
    }
}
