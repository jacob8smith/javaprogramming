package chapter5;

import java.util.Scanner;

public class Ex4 {

    public static void main(String[] args) {

        /*
        (Conversion from miles to kilometers) Write a program that displays the following
        table (note that 1 mile is 1.609 kilometers):
         */

        Scanner input = new Scanner(System.in);

        System.out.println("Miles             Kilometers");

        for (int i = 1; i <= 10; i++) {

            System.out.println(i + "                      " + (i * 1.609));

        }

    }
}
