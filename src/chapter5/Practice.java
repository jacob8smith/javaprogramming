package chapter5;

public class Practice {

    public static void main(String[] args) {

        int count = 0;

        while (count < 5) {
            System.out.println("This should only repeat five times.");
            count++;
        }

        for (int i = 0; i <10; i++) {
            System.out.println("Test GitLab Time Zone Part 2");
        }

    }
}
